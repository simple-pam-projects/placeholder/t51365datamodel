package com.example.t51365.model;

import java.util.ArrayList;

public class ApprovalList {

    ArrayList<ApprovalGroup> groupList;


    public ApprovalList() {
        groupList = new ArrayList<>();
    }

    public ApprovalList(ArrayList<ApprovalGroup> groupList) {
        this.groupList = groupList;
    }

    public ArrayList<ApprovalGroup> getGroupList() {
        return this.groupList;
    }

    public void setGroupList(ArrayList<ApprovalGroup> groupList) {
        this.groupList = groupList;
    }

    public void addApprovalGroup(ApprovalGroup ag) {
        if (this.groupList==null) {
            this.groupList = new ArrayList<>();
        }
        groupList.add(ag);
    }

    @Override
    public String toString() {
        return "{" +
            " groupList='" + getGroupList() + "'" +
            "}";
    }

    
}
