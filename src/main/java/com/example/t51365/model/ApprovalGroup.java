package com.example.t51365.model;

public class ApprovalGroup {

    Integer numberOfApprovals;
    String groupName;


    public ApprovalGroup() {
    }

    public ApprovalGroup(Integer numberOfApprovals, String groupName) {
        this.numberOfApprovals = numberOfApprovals;
        this.groupName = groupName;
    }

    public Integer getNumberOfApprovals() {
        return this.numberOfApprovals;
    }

    public void setNumberOfApprovals(Integer numberOfApprovals) {
        this.numberOfApprovals = numberOfApprovals;
    }

    public String getGroupName() {
        return this.groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public ApprovalGroup numberOfApprovals(Integer numberOfApprovals) {
        setNumberOfApprovals(numberOfApprovals);
        return this;
    }

    public ApprovalGroup groupName(String groupName) {
        setGroupName(groupName);
        return this;
    }

    @Override
    public String toString() {
        return "{" +
            " numberOfApprovals='" + getNumberOfApprovals() + "'" +
            ", groupName='" + getGroupName() + "'" +
            "}";
    }

    
}
