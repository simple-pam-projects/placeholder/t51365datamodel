package com.example.t51365.model;

import java.util.ArrayList;

public class ApprovalRequest {

    Integer requestNumber;
    String requestService;
    String requestAction;
    ArrayList<RowData> rowData;
    String decision;

    public ApprovalRequest() {
    }

    public ApprovalRequest(Integer requestNumber, ArrayList<RowData> rowData, String decision) {
        this.requestNumber = requestNumber;
        this.rowData = rowData;
        this.decision = decision;
    }

    public Integer getRequestNumber() {
        return this.requestNumber;
    }

    public void setRequestNumber(Integer requestNumber) {
        this.requestNumber = requestNumber;
    }

    public ArrayList<RowData> getRowData() {
        return this.rowData;
    }

    public void setRowData(ArrayList<RowData> rowData) {
        this.rowData = rowData;
    }

    public String getDecision() {
        return this.decision;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    public ApprovalRequest requestNumber(Integer requestNumber) {
        setRequestNumber(requestNumber);
        return this;
    }

    public ApprovalRequest rowData(ArrayList<RowData> rowData) {
        setRowData(rowData);
        return this;
    }

    public ApprovalRequest decision(String decision) {
        setDecision(decision);
        return this;
    }

    public String getRequestService() {
        return this.requestService;
    }

    public void setRequestService(String requestService) {
        this.requestService = requestService;
    }

    public String getRequestAction() {
        return this.requestAction;
    }

    public void setRequestAction(String requestAction) {
        this.requestAction = requestAction;
    }

    @Override
    public String toString() {
        return "{" +
                " requestNumber='" + getRequestNumber() + "'" +
                ", requestService='" + getRequestService() + "'" +
                ", requestAction='" + getRequestAction() + "'" +
                ", rowData='" + getRowData() + "'" +
                ", decision='" + getDecision() + "'" +
                "}";
    }

}
